package policies.returnDID

_ = {
     "@context" : ["https://www.w3.org/ns/did/v1", "https://w3id.org/security/suites/jws-2020/v1"],
     "id" :  external.http.header("X-Did-Location"),
     "verificationMethod" : verification_methods(external.http.header("X-Did-Location"), external.http.header("X-Did-Transit-Engine"))
}